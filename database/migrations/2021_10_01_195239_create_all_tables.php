<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->string('nome', 200);
            $table->string('email',200);
            $table->string('foto',200)->nullable();
            $table->string('tipo',2);
            $table->timestamps();
        });

        Schema::create('clientes_telefones', function (Blueprint $table) {
            $table->id();
            $table->string('id_cliente', 11);
            $table->string('id_telefone', 11);
        });

        Schema::create('telefones', function (Blueprint $table) {
            $table->id();
            $table->string('telefone',13);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
        Schema::dropIfExists('clientes_telefones');
        Schema::dropIfExists('telefones');
    }
}
