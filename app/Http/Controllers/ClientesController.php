<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\boasVindas;
use App\Models\Cliente;
use App\Models\Clientes_telefone;
use App\Models\Telefone;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class ClientesController extends Controller
{

    
    public function add(){


        return view('clientes.add',[

            'botao' => 'Cadastrar'
        
        ]);

        

    }

    public function addAction(Request $request){


        $request->validate([

            'nome' => ['required', 'string'],
            'email' => ['required', 'email', 'unique:clientes,email'],
            'tipo' => ['required', 'integer'],
            'foto' => ['mimes:jpeg,jpg,png','required','max:10000'] // max 10000kb

            

        ]);

        $nomeFoto = $this->adicionarFoto($request);


        if(!$c= Cliente::where('email', $request->email)->first()){

            $c = new Cliente;
            $c->nome = $request->nome;
            $c->email = $request->email;
            $c->tipo = $request->tipo;
            $c->foto = $nomeFoto;
            $c->save();

        }

        Clientes_telefone::where('id_cliente',$c->id)->delete();

        foreach($request->telefone as $telefone){

            

            
            if(!$f = Telefone::where('telefone', $telefone)->first()){

                $f = new Telefone;
                $f->telefone = $telefone;
                $f->save();


            }

            

            if(!Clientes_telefone::where([
                
                'id_cliente' => $c->id,
                'id_telefone' => $f->id
                
                
                ])->first()){

                $cf = new Clientes_telefone;
                $cf->id_cliente = $c->id;
                $cf->id_telefone = $f->id;
                $cf->save();


            }


        }

        Mail::to($c->email)->send(new boasVindas($c));

        return Redirect::route('clientes.listar')->with(['success' => 'Cadastro concluído com sucesso']);

    }


    public function listar(){

        $clientes = Cliente::all();

        return view('clientes.listar',compact('clientes'));

    }

    public function edit($id){

        $data = Cliente::find($id);


        $telefones = Clientes_telefone::where('id_cliente', $id)
        ->join('telefones', 'telefones.id', '=', 'clientes_telefones.id_telefone')
        ->get();


    
        if($data){

            return view('clientes.edit', [
                'data' => $data,
                'telefones' => $telefones,
                'botao' => 'Alterar'
            ]);

        }else{

            return redirect()->route('clientes.listar');

        }


    }

    public function editAction(Request $request, $id){

        $c = Cliente::find($id);

        $request->validate([

            'nome' => ['required', 'string'],
            'email' => ['required', 'email'],
            'tipo' => ['required', 'integer'],
            
        ]);

        if($request->foto){

            $request->validate([

                'foto' => ['mimes:jpeg,jpg,png','required','max:10000'] // max 10000kb
                
    
            ]);

            $nomeFoto = $this->adicionarFoto($request);
    
        }else{

            $nomeFoto = $c->foto;

        }

    
        if($c){

            // Pega os dados do cliente 
            $c = Cliente::find($id);

            $c->nome = $request->nome;
            $c->email = $request->email;
            $c->tipo = $request->tipo;
            $c->foto = $nomeFoto;
            $c->save();

        }

        Clientes_telefone::where('id_cliente',$c->id)->delete();

        foreach($request->telefone as $telefone){
            
            if(!$f = Telefone::where('telefone', $telefone)->first()){

                $f = new Telefone;
                $f->telefone = $telefone;
                $f->save();


            }

            if(!Clientes_telefone::where([
                
                'id_cliente' => $c->id,
                'id_telefone' => $f->id
                
                
                ])->first()){

                $cf = new Clientes_telefone;
                $cf->id_cliente = $c->id;
                $cf->id_telefone = $f->id;
                $cf->save();


            }


        }


        return Redirect::route('clientes.listar')->with(['success' => 'Cadastro editado com sucesso']);


    }    
    
    public function del($id){

        Cliente::find($id)->delete();


        return redirect()->route('clientes.listar');
    }

    public function adicionarFoto(Request $request){

        //Upload de foto
        if($request->hasFile('foto')){
            // Get filename with the extension
            $filenameWithExt = $request->file('foto')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('foto')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore= $filename.'_'.time().'.'.$extension;
            // Upload Image
            $path = $request->file('foto')->storeAs('', $fileNameToStore, 'foto');


            //dd($path);
            
        } else {
            $fileNameToStore = 'noFoto.png';
        }

            return $fileNameToStore;

    } 



}
