
## 1.Configuração do projeto

Na pasta do projeto execute os comandos abaixo no terminal.

## 1.1 Instalação do composer

- sudo composer install


## 1.2 Configuração de acesso ao banco de dados

Em seguida configure as variáveis de acesso ao banco de dados de acordo com as informações de seu banco, renomeando o arquivo env.example para .env, dentro do arquivo altere as variáveis, exemplo;

    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=teste
    DB_USERNAME=root
    DB_PASSWORD=123


## 1.3 Configuração de envio de e-mails

Configure as variáveis do servidor de e-mail de acordo com a sua escolha, exemplo;

    MAIL_MAILER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=0fb2fddf6a2435
    MAIL_PASSWORD=66b5dbf6df1c65
    MAIL_ENCRYPTION=tls
    MAIL_FROM_ADDRESS=null
    MAIL_FROM_NAME="${APP_NAME}"

## 1.4 Configuração do banco de dados

Logo após instalar o composer, rode o comando abaixo para criar o banco de dados.

- php artisan migrate:fresh


## 1.5 Configuração para armazenamento de imagens

Depois de configurar as varíavies de envio de e-mail, é necessário criar uma pasta chamada foto dentro de public/, exemplo;

- public/foto

## 1.6 Gerar de chave JWT 

Na pasta do projeto execute o comando abaixo para gerar a chave do JWT.


 - php artisan jwt:secret
 - php artisan jwt:secret


## 2.Instruções de uso da API

- E-mail e senha: conta@crud.com | teste321

Rotas de acesso 

  //Rota para logar na api
- Login: http://127.0.0.1:8000/api/auth/login

  //Rota para pegar todos os clientes
- Clientes: http://127.0.0.1:8000/api/auth/clientes

  //Rota para pegar o cliente por ID
- Cliente por ID: http://127.0.0.1:8000/api/auth/cliente/{id_cliente} 


## 3.Sobre a aplicação
O objetivo da aplicação é criar um CRUD de uma tabela com as seguintes caracterísicas:

- Disparo de e-mail
- Utilização de migrations para a criação de tabelas
- Utilizar Eloquent para os relacionamentos
- Disponibilizar os dados de clientes via API com autenticação JWT


## 4.Materiais de referência

- Curso da B7WEB.
- Aluno

- Médiun
- https://medium.com/@jiyose12/criando-upload-de-arquivos-em-laravel-b80423d3e129

- Especializa TI.
- https://blog.especializati.com.br/envio-de-e-mails-no-laravel/

- Paulo RB
- https://paulorb.net/autenticacao-jwt-laravel-8-direto-ao-ponto/

- Stack Overflow
- Para as demais dúvidas 
