<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClientesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[ClientesController::class, 'listar']);//Listagem de clientes


Route::prefix('/clientes')->group(function(){

    Route::get('/',[ClientesController::class, 'listar'])->name('clientes.listar');//Listagem de clientes

    Route::get('add',[ClientesController::class, 'add'])->name('clientes.add');// Tela de adição de cliente
    Route::post('add',[ClientesController::class, 'addAction']);//Ação de adição de novo cliente

    Route::get('edit/{id}',[ClientesController::class, 'edit'])->name('clientes.edit'); //Tela de edição de cliente 
    Route::post('edit/{id}',[ClientesController::class, 'editAction']);//Ação de edição de cliente

    Route::get('del/{id}',[ClientesController::class, 'del'])->name('clientes.del');//Ação de deletar cliente

});


Route::fallback(function(){

   // return view('clientes.listar');

});
