@extends('layouts.admin')

@section('title','Edição de clientes')

@section('content')
<h1 class="text-center">Edição de cliente</h1>
<br>
<br>
<br>
  <form method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label for="nome">Nome</label>
          <input type="text" class="form-control" name="nome" id="nome" placeholder="Informe seu nome" value="{{$data->nome}}" >      
          @if($errors->has('nome'))
            <div class="alert alert-danger">{{ $errors->first('nome') }}</div>
          @endif
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label for="email">Email</label>
          <input type="text" name="email" class="form-control" id="email" placeholder="Informe seu e-mail" value="{{$data->email}}">
          @if($errors->has('email'))
            <div class="alert alert-danger">{{ $errors->first('email') }}</div>
          @endif
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="form-group">
          <label for="tipo">Tipo</label>
          <select class="form-control" name="tipo" id="tipo">
            <option value="0" 
            @if($data->tipo == 0)
            
            {{'selected=selected'}} 
            
            @endif>Pessoa física</option>
            <option value="1" 
            @if($data->tipo == 1)

            {{'selected=selected'}}
            
            @endif>Pessoa jurídica</option>
          </select>
          @if($errors->has('tipo'))
            <div class="alert alert-danger">{{ $errors->first('tipo') }}</div>
          @endif
        </div>
      </div>
      <div class="col-md-3">
        <div class="form-group">
          <input type="file" name="foto" placeholder="Selecione a imagem" style="padding-top: 40px;">
          @if($errors->has('foto'))
            <div class="alert alert-danger">{{ $errors->first('foto') }}</div>
          @endif
        </div>
      </div>
    </div>

    @if(count($telefones) > 0)
     
      <div id="grupo-telefone">
        @foreach ($telefones as $key => $linhaTelefone)

        <div class="row" id="{{'telefone'.$key}}">
          <div class="col-md-3">
            <div class="form-group">
              Telefone
              <input type="text" class="telefone" name="telefone[{{$key}}]" value="{{$linhaTelefone->telefone}}">
              <button type="button" class="btn btn-danger btn-sm btn-apagar"  id="{{$key}}">X</button>
            </div>
          </div>
        </div>
        @endforeach
      </div>
    @else
      <div id="grupo-telefone">
        <div class="row">
          <div class="col-md-3">
            <div class="form-group">
              Telefone
              <input type="text" class="telefone" name="telefone[1]">
              <button type="button" class="btn btn-danger btn-sm btn-apagar"  id="1">X</button>
            </div>
          </div>
        </div>
      </div>
    @endif  
    <div class="row">
      <div class="col-md-2">
        <button type="button" class="btn btn-outline-primary" value="Adicionar recarga" id="add-campo">Adicionar telefone</button>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-1">
        <input type="submit" value="{{$botao}}" class="btn btn-success" style="text-align:center;">
      </div>
    </div>
  </form>
  <br>
  <br>
@endsection