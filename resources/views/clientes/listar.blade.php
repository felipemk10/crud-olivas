@extends('layouts.admin')

@section('title','Cadastro de clientes')

@section('content')
  <h1 class="text-center">Listagem de clientes</h1>
  <br>
  <br>
  <br>
  @if(Session::has('success'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('success') }}</p>
  @endif
  <div class="row">
    <div class="col-md-12">
      <a href="{{route('clientes.add')}}" type="button" class="btn btn-outline-primary">Cadastrar cliente
      </a>
    </div>
  </div>
  <br>
  <br>
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Nome</th>
        <th scope="col">Email</th>
        <th scope="col">Tipo</th>
        <th scope="col">Ações</th>
      </tr>
    </thead>
    <tbody>
      @if(count($clientes) > 0)
        @foreach($clientes as $cliente)
          <tr>
            <th scope="row">{{$cliente->id}}</th>
            <td>{{$cliente->nome}}</td>
            <td>{{$cliente->email}}</td>
            <td>
              @if($cliente->tipo == '0')
                {{"Pessoa Física"}}
              @else
              {{"Pessoa Jurídica"}}
              @endif
            </td>
            <td>
              <a type="button" class="btn btn-primary" href="{{route('clientes.edit',['id'=>$cliente->id])}}">Alterar</a>
              <a type="button" class="btn btn-danger" href="{{route('clientes.del',['id'=>$cliente->id])}}">Excluir</a>
            </td>
          </tr>
        @endforeach
      @else
      <tr>{{"Não há clientes para listar"}}</tr>
      @endif
    </tbody>
  </table>
@endsection