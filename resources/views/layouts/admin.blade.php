<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>@yield('title') - Processo seletivo</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
	
</head>
<body>
	<header>
		<nav class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
			<a class="navbar-brand col-md-3 col-lg-2 mr-0 px-3" href="https://www.olivas.digital/">Olivas Digital</a>
		  </nav>
	</header>
	<br>
	<section>
		<div class="container">
			@yield('content')
		</div>
	</section>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
	<!--<script src="https://cdnjs.com/libraries/jquery.mask"></script>-->
	<script type="text/javascript">
		console.log("Legal");

		//$('.telefone').mask('(99) 99999-9999');


		var cont = 1000;
	
		$("#add-campo").click(function() {



			console.log("Legal");


			$("#grupo-telefone").append('<div class="row" id="telefone'+ cont +'"><div class="col-md-3"><div class="form-group">Telefone<input type="text" class="telefone" name="telefone['+cont+']"><button type="button" class="btn btn-danger btn-sm btn-apagar" id="'+ cont +'">X</button></div></div></div>');


						
			});// Add campo

			$('#grupo-telefone').on('click', '.btn-apagar', function () {

			var button_id = $(this).attr("id");
			console.log("Apagou");

			$('#telefone' + button_id + '').remove();

			});



	</script>
</body>
</html>